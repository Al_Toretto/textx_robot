from textx import metamodel_from_file

robot_mm = metamodel_from_file('grammer.tx')

def move_command_processor(move_cmd):
    if move_cmd.steps == 0:
        move_cmd.steps = 1
      

robot_mm.register_obj_processors({'MoveCommand': move_command_processor})



class Robot(object):
    def __init__(self) -> None:
        self.x = 0
        self.y = 0

    def __str__(self):
        return f"Robot position is {self.x}, {self.y}."
    
    def interpret(self, model):
        for command in model.commands:
            if command.__class__.__name__ == "InitialCommand":
                print(f"Setting Position to: {command.x}, {command.y}")
                self.x = command.x
                self.y = command.y
            else:
                print(f"Going {command.direction} for {command.steps} steps.")

                move_vector = {"up": (0, 1),
                        "down": (0, -1),
                        "left": (-1, 0),
                        "right": (1, 0)}[command.direction]
                
                self.x += command.steps * move_vector[0]
                self.y += command.steps * move_vector[1]
            

            print(self)



robot_model = robot_mm.model_from_file('program.robot')

robot = Robot()

robot.interpret(robot_model)